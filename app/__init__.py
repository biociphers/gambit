import logging
from logging.handlers import SMTPHandler, RotatingFileHandler
import os
from flask import Flask, request, current_app
from flask_sqlalchemy import SQLAlchemy
from flask_login import LoginManager
from flask_mail import Mail
from flask_bootstrap import Bootstrap
from flask_moment import Moment
from redis import Redis
import rq
from config import Config
from flask_session import Session

db = SQLAlchemy()

login = LoginManager()
login.login_view = 'auth.login'
login.login_message = 'Please log in to access this page.'
mail = Mail()
bootstrap = Bootstrap()
moment = Moment()


from sqlalchemy.event import listen

def create_app(config_class=Config):
    adtl_confs = {}
    if hasattr(config_class, 'EXT_STATIC_FOLDER'):
        adtl_confs['static_folder'] = config_class.EXT_STATIC_FOLDER
    if hasattr(config_class, 'EXT_STATIC_URL_PATH'):
        adtl_confs['static_url_path'] = config_class.EXT_STATIC_URL_PATH
    app = Flask(__name__, **adtl_confs)
    app.config.from_object(config_class)

    sess = Session()
    sess.init_app(app)

    db.init_app(app)


    login.init_app(app)
    mail.init_app(app)
    bootstrap.init_app(app)
    moment.init_app(app)

    app.redis = Redis.from_url(app.config['REDIS_URL'])
    app.task_queue = rq.Queue('CATs-tasks', connection=app.redis)



    from app.errors import bp as errors_bp
    app.register_blueprint(errors_bp, url_prefix=app.config.get('EXT_URL_PREFIX', ''))

    from app.auth import bp as auth_bp
    app.register_blueprint(auth_bp, url_prefix=app.config.get('EXT_URL_PREFIX', '') + '/auth')

    from app.main import bp as main_bp
    app.register_blueprint(main_bp, url_prefix=app.config.get('EXT_URL_PREFIX', ''))
    #
    # from app.api import bp as api_bp
    # app.register_blueprint(api_bp, url_prefix='/api')

    if not app.debug and not app.testing:
        if app.config['MAIL_SERVER']:
            auth = None
            if app.config['MAIL_USERNAME'] or app.config['MAIL_PASSWORD']:
                auth = (app.config['MAIL_USERNAME'],
                        app.config['MAIL_PASSWORD'])
            secure = None
            if app.config['MAIL_USE_TLS']:
                secure = ()
            mail_handler = SMTPHandler(
                mailhost=(app.config['MAIL_SERVER'], app.config['MAIL_PORT']),
                fromaddr='no-reply@' + app.config['MAIL_SERVER'],
                toaddrs=app.config['ADMINS'], subject='CATs Failure',
                credentials=auth, secure=secure)
            mail_handler.setLevel(logging.ERROR)
            app.logger.addHandler(mail_handler)

        if app.config['LOG_TO_STDOUT']:
            stream_handler = logging.StreamHandler()
            stream_handler.setLevel(logging.INFO)
            app.logger.addHandler(stream_handler)
        else:
            if not os.path.exists('logs'):
                os.mkdir('logs')
            file_handler = RotatingFileHandler('logs/rbp-pokedex.log',
                                               maxBytes=10240, backupCount=10)
            file_handler.setFormatter(logging.Formatter(
                '%(asctime)s %(levelname)s: %(message)s '
                '[in %(pathname)s:%(lineno)d]'))
            file_handler.setLevel(logging.INFO)
            app.logger.addHandler(file_handler)

        app.logger.setLevel(logging.INFO)
        app.logger.info('Gambit startup')

    return app



from app import models
