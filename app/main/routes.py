from datetime import datetime
from flask import render_template, flash, redirect, url_for, request, g, \
    jsonify, current_app, session, send_file
from flask_login import current_user, login_required
from sqlalchemy import func, case, text
from sqlalchemy.types import DECIMAL
from app import db
from app.models import Tissue_RBP
from app.main import bp
import csv, io
from collections import OrderedDict
from urllib.parse import urlencode


# @bp.before_app_request
# def before_request():
#     if current_user.is_authenticated:
#         current_user.last_seen = datetime.utcnow()
#         db.session.commit()
#         g.search_form = SearchForm()


import string, random
from itertools import chain

@bp.route('/', methods=['GET'])
@bp.route('/index', methods=['GET'])
def index():

    return render_template('index.html', title='Overview')

import numpy as np
from scipy.cluster.hierarchy import linkage, dendrogram
from scipy.spatial.distance import pdist

import json
import os
import tempfile
import subprocess

@bp.route('/json/datatest', methods=['POST'])
def datatest():

    with open(os.path.join(os.path.dirname(__file__), 'clustered.json'), 'r') as f:
        final_json = json.loads(f.read())

    return jsonify(final_json)

@bp.route('/json/data_proc', methods=['POST'])
def data_proc():

    # Section from David
    input_npy = io.BytesIO(request.files['input_matrix'].read())
    output_npy = io.BytesIO(request.files['output_matrix'].read())

    #input_npy = '/home/pjewell/Downloads/beatAML_chessboard_n2_in.npz'
    #output_npy = '/home/pjewell/Downloads/beatAML_chessboard_n2_out.npz'





    inp_mat = np.load(input_npy)
    psi_mat = inp_mat['x'][0, :, :] / (inp_mat['x'][0, :, :] + inp_mat['x'][1, :, :])

    out_mat = np.load(output_npy)

    labels_y = inp_mat['lsv']
    labels_x = inp_mat['samples']

    c = np.sort(out_mat["c"]).astype(int)
    r_bounding = out_mat["r"][:, out_mat["c"].astype(int)]
    pfg = out_mat["prob_fg"]
    ppw = out_mat["prob_pw"]



    #colorder = np.argsort(out_mat["c"])
    #roworder = np.array(dendrogram(linkage(pdist(pfg)), no_plot=True)["ivl"], dtype=int)

    # re order based on specified ordering
    #psi_mat = psi_mat[:, colorder][roworder]

    # print(labels_x[:10])
    # print(labels_y[:10])
    # labels_x = labels_x[roworder]
    # labels_y = labels_y[colorder]
    # print(labels_x[:10])
    # print(labels_y[:10])

    """
    new part?
    """
    ppw_labs = (ppw >= 0.6).astype(int)
    clust_id = np.unique(ppw_labs, axis=0)
    new_c = np.zeros(c.shape[0])
    for i in range(clust_id.shape[0]):
        for j in range(c.shape[0]):
            if np.all(clust_id[i, :] == ppw_labs[j, :]):
                new_c[j] = i
    c = np.sort(new_c)
    colorder = np.argsort(new_c)
    roworder = np.array(dendrogram(linkage(pdist(pfg)), no_plot=True)["ivl"], dtype=int)


    # end new part
    #print(c)

    #cluster_lens = []
    #last_poition = 0
    for x in np.unique(r_bounding, axis=0):
        #print('unique r', len(x))
        idx = []
        for i in range(psi_mat.shape[0]):
            if np.all(r_bounding[i, :] == x):
                idx.append(i)
        tmp = psi_mat[idx, :]
        tmp2 = tmp[:, x == 1]
        # [[0.1,0.2,0.3],[0.4,0.5,0.6],[0.7,0.8,0.9]], ["A", "B", "C", ], ["v1", "v2", "v3"]
        #cluster_lens.append([labels_x[last_poition], labels_x[last_poition + len(tmp) - 1]])
        #last_poition += len(tmp)
        if np.sum(x) > 0:
            sort = np.argsort(np.nanmean(tmp2, axis=1))
            tmp = tmp[sort, :]
            psi_mat[idx, :] = tmp

    psi_mat = psi_mat[:, colorder][roworder]


    r_sort = r_bounding[:, colorder][roworder]
    np.savetxt('/tmp/test1.tsv', r_sort, fmt='%i', delimiter='\t')

    #return jsonify({})


    # section to convert to inchlib format
    # tmp_input = tempfile.mktemp(suffix='.csv')
    # tmp_output = tempfile.mktemp(suffix='.json')

    # print("sending")
    # print(cluster_lens)

    print(len(labels_x), len(c))
    # cluster_lens = []
    # last_position = 0
    # current_n = c[0]
    # for i, n in enumerate(c):
    #
    #     if n != current_n:
    #         print(n, current_n)
    #         current_n = n
    #         cluster_lens.append([last_position, i - last_position - 1])
    #         last_position = i
    # cluster_lens.append([cluster_lens[-1][1] + 1, len(c)-1])
    #
    # print(cluster_lens)
    # #cluster_lens = [[0, 3], [3, 200], [200, 476]]
    # cluster_lens = [[0, 2], [3, 211], [212, 476]]
    # cluster_lens = [ [labels_x[l[0]], labels_x[l[1]]] for l in cluster_lens ]
    print(np.count_nonzero(c == 1), np.count_nonzero(c == 2))

    c_val = 0
    cluster_lens = []
    while True:
        count = np.count_nonzero(c == c_val)
        print('found', count, c_val)
        if count == 0:
            break

        if not cluster_lens:
            cluster_lens.append([0, count-1])
        else:
            cluster_lens.append([cluster_lens[-1][1]+1, min(cluster_lens[-1][1] + count, len(c)-1)])

        c_val += 1

    #cluster_lens = [[0, 2], [3, 214], [215, 476]]

    print(cluster_lens)

    cluster_lens = [[labels_x[l[0]], labels_x[l[1]]] for l in cluster_lens]
    print(psi_mat.shape)


    psi_mat = np.nan_to_num(psi_mat, copy=False)


    return jsonify({'rawdata': psi_mat.transpose().tolist(), 'cluster_lens': cluster_lens, 'x_labels':labels_x.tolist(),
                    'y_labels': labels_y.tolist()})

    # with open(tmp_input, 'w') as f:
    #     f.write(' ,' + ','.join(labels_y) + '\n')
    #     for i, row in enumerate(psi_mat):
    #         f.write(labels_x[i] + ',' + ','.join((str(x) for x in row)).replace('nan', 'N/A') + '\n')
    #
    # path_to_inchlib_env = os.path.join(os.path.dirname(os.path.dirname(os.path.dirname(__file__))), 'env_2.7', 'bin', 'activate')
    # path_to_inchlib = os.path.join(os.path.dirname(os.path.dirname(os.path.dirname(__file__))), 'inchlib_clust.py')
    # #args = ('python', path_to_inchlib, '-o', tmp_output, '-dh', '-mv', 'N/A', tmp_input)
    # shell_line = f". {path_to_inchlib_env}; python {path_to_inchlib} -o {tmp_output} -dh -mv 'N/A' {tmp_input}"
    # subprocess.call(shell_line, shell=True)
    #
    # with open(tmp_output, 'r') as f:
    #     final_json = json.loads(f.read())
    #
    # return jsonify(final_json)



@bp.route('/json/process_numpy', methods=['POST'])
def process_numpy():

    """
    Get back a list of (RBP,relation_rank) related to tissue / rbp1
    """

    # input_npy = request.form['input_npy']
    # output_npy = request.form['output_npy']
    #
    #
    # inp_mat = np.load(input_npy)['x']
    # psi_mat = inp_mat[0, :, :] / (inp_mat[0, :, :] + inp_mat[1, :, :])
    #
    # out_mat = np.load(output_npy)

    final_json = json.load('/home/pjewell/PycharmProjects/gambit/clustered.json')

    # with open('/home/pjewell/PycharmProjects/gambit/clustered.json', 'r') as f:
    #     final_json = json.loads(f.read())

    return jsonify(final_json)




    inp_mat = np.load("/home/pjewell/Downloads/beatAML_chessboard.npz")
    psi_mat = inp_mat['x'][0, :, :] / (inp_mat['x'][0, :, :] + inp_mat['x'][1, :, :])

    print([x for x in inp_mat])

    out_mat = np.load("/home/pjewell/Downloads/beatAML_chessboard_out.npz")
    print([x for x in out_mat])

    labels_x = inp_mat['lsv']
    labels_y = inp_mat['samples']

    c = np.sort(out_mat["c"]).astype(int)
    r = out_mat["r"][:, out_mat["c"].astype(int)]
    pfg = out_mat["prob_fg"]
    ppw = out_mat["prob_pw"]

    colorder = np.argsort(out_mat["c"])
    roworder = np.array(dendrogram(linkage(pdist(pfg)), no_plot=True)["ivl"], dtype=int)

    for x in np.unique(r, axis=0):
        idx = []
        for i in range(psi_mat.shape[0]):
            if np.all(r[i, :] == x):
                idx.append(i)
        tmp = psi_mat[idx, :]
        tmp2 = tmp[:, x == 1]
        if np.sum(x) > 0:
            sort = np.argsort(np.nanmean(tmp2, axis=1))
            tmp = tmp[sort, :]
            psi_mat[idx, :] = tmp

    json_out = {
        "data": {
            "nodes": {

            }
        }
    }

    for i, row in enumerate(psi_mat):
        pass
