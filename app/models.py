import base64
from datetime import datetime, timedelta
from hashlib import md5
import json
import os
from time import time
from flask import current_app, url_for
from flask_login import UserMixin
from werkzeug.security import generate_password_hash, check_password_hash
import jwt
from app import db

#
# class Genes_Tissues(db.Model):
#     __tablename__ = 'genes_tissues'
#
#     id = db.Column(db.Integer, primary_key=True)
#     gene_name = db.Column(db.String, index=True)
#     tissue = db.Column(db.String, index=True)
#     num_filtered = db.Column(db.Integer, index=True)
#
#     TPM_median_nonCAT = db.Column(db.Float)
#     TPM_median_CAT_max = db.Column(db.Float)
#     pathogenic = db.Column(db.Boolean)
#     inadequate_whole_blood = db.Column(db.Integer, index=True)
#     inadequate_fibroblasts = db.Column(db.Integer, index=True)
#     inadequate_lymphocytes = db.Column(db.Integer, index=True)
#
class Tissue_RBP(db.Model):
    __tablename__ = 'tissue_rbp'

    id = db.Column(db.Integer, primary_key=True)
    tissue = db.Column(db.String(41), index=True)
    rbp1 = db.Column(db.String(9), index=True)
    rbp2 = db.Column(db.String(9), index=True)
    result = db.Column(db.Float, index=True)
    ctrl_tpm = db.Column(db.Float)
    kd_tpm = db.Column(db.Float)

#
# class User(UserMixin, db.Model):
#     id = db.Column(db.Integer, primary_key=True)
#     username = db.Column(db.String(64), index=True, unique=True)
#     email = db.Column(db.String(120), index=True, unique=True)
#     password_hash = db.Column(db.String(128))
#     posts = db.relationship('Post', backref='author', lazy='dynamic')
#     about_me = db.Column(db.String(140))
#     last_seen = db.Column(db.DateTime, default=datetime.utcnow)
#     token = db.Column(db.String(32), index=True, unique=True)
#     token_expiration = db.Column(db.DateTime)
#
#     messages_sent = db.relationship('Message',
#                                     foreign_keys='Message.sender_id',
#                                     backref='author', lazy='dynamic')
#     messages_received = db.relationship('Message',
#                                         foreign_keys='Message.recipient_id',
#                                         backref='recipient', lazy='dynamic')
#     last_message_read_time = db.Column(db.DateTime)
#     notifications = db.relationship('Notification', backref='user',
#                                     lazy='dynamic')
#     tasks = db.relationship('Task', backref='user', lazy='dynamic')
#
#     def __repr__(self):
#         return '<User {}>'.format(self.username)
#
#     def set_password(self, password):
#         self.password_hash = generate_password_hash(password)
#
#     def check_password(self, password):
#         return check_password_hash(self.password_hash, password)
#
#     def avatar(self, size):
#         digest = md5(self.email.lower().encode('utf-8')).hexdigest()
#         return 'https://www.gravatar.com/avatar/{}?d=identicon&s={}'.format(
#             digest, size)
#
#     def get_reset_password_token(self, expires_in=600):
#         return jwt.encode(
#             {'reset_password': self.id, 'exp': time() + expires_in},
#             current_app.config['SECRET_KEY'],
#             algorithm='HS256').decode('utf-8')
#
#     @staticmethod
#     def verify_reset_password_token(token):
#         try:
#             id = jwt.decode(token, current_app.config['SECRET_KEY'],
#                             algorithms=['HS256'])['reset_password']
#         except:
#             return
#         return User.query.get(id)
#
#
#     def to_dict(self, include_email=False):
#         data = {
#             'id': self.id,
#             'username': self.username,
#             'last_seen': self.last_seen.isoformat() + 'Z',
#             'about_me': self.about_me,
#             'post_count': self.posts.count(),
#             'follower_count': self.followers.count(),
#             'followed_count': self.followed.count(),
#             '_links': {
#                 'self': url_for('api.get_user', id=self.id),
#                 'followers': url_for('api.get_followers', id=self.id),
#                 'followed': url_for('api.get_followed', id=self.id),
#                 'avatar': self.avatar(128)
#             }
#         }
#         if include_email:
#             data['email'] = self.email
#         return data
#
#     def from_dict(self, data, new_user=False):
#         for field in ['username', 'email', 'about_me']:
#             if field in data:
#                 setattr(self, field, data[field])
#         if new_user and 'password' in data:
#             self.set_password(data['password'])
#
#     def get_token(self, expires_in=3600):
#         now = datetime.utcnow()
#         if self.token and self.token_expiration > now + timedelta(seconds=60):
#             return self.token
#         self.token = base64.b64encode(os.urandom(24)).decode('utf-8')
#         self.token_expiration = now + timedelta(seconds=expires_in)
#         db.session.add(self)
#         return self.token
#
#     def revoke_token(self):
#         self.token_expiration = datetime.utcnow() - timedelta(seconds=1)
#
#     @staticmethod
#     def check_token(token):
#         user = User.query.filter_by(token=token).first()
#         if user is None or user.token_expiration < datetime.utcnow():
#             return None
#         return user
#
#
# @login.user_loader
# def load_user(id):
#     return User.query.get(int(id))
#
