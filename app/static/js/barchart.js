
// Bar chart

function plot_barchart(data_incoming) {

	$('#barplot').children().remove();
	$('#barplot').append('<div id="barplot-side" class="col-md-6" style="padding: 0;">');
	$('#barplot').append('<div id="barplot-main" class="col-md-6" style="padding: 0;">');


	/*
		--- Side Plot
	 */

	function side_plot() {
		let data = [];
		for (let val of data_incoming['res']) {
			data.push({name: val[0], value: Math.asinh(val[2])});
		}

		const header_height = 70;

		var margin = {top: 10, right: 50, bottom: 0, left: 50},
			width = ($('#barplot').parent().width() * (1 / 2)) - margin.left - margin.right,
			height = 700 - margin.top - margin.bottom + header_height;

		// var div = d3.select("#barplot-side").append("div")
		// 	.attr("class", "tooltip")
		// 	.style("background-color", "rgba(191,118,255,0.95)")
		// 	.style("font-size", "18px")
		// 	.style("border-radius", "5px")
		// 	.style("padding", "2px")
		// 	.style("opacity", 0);

		var outer = d3.select("#barplot-side").append("svg")
			.attr("width", width + margin.left + margin.right)
			.attr("height", height + margin.top + margin.bottom + header_height);

		var header = outer.append("g")
			.attr("transform", "translate(" + margin.left + "," + margin.top + ")");

		var svg = outer.append("g")
			.attr("transform", "translate(" + margin.left + "," + (margin.top + header_height) + ")");

		var x = d3.scaleLinear()
			.range([0, width]);

		const max_val = d3.max(data, function (d) {
			return Math.abs(d.value)
		});

		var y = d3.scaleBand()
			.rangeRound([height, 0])
			.padding(0.2);

		// data.sort(function (a, b) {
		// 	return b.value - a.value;
		// });


		x.domain([-1 * max_val, max_val]);
		y.domain(data.map(function (d) {
			return d.name;
		}));

		svg.selectAll(".connectline")
			.data(data)
			.enter().append("line")
			.attr("x1", x(0))
			.attr("x2", 2000)
			.attr("y1", function (d) {
				return y(d.name) + (y.bandwidth() / 2);
			})
			.attr("y2", function (d) {
				return y(d.name) + (y.bandwidth() / 2);
			})
			.attr("stroke", "#3a403d")
			.attr("stroke-width", "1px")
			.attr("stroke-dasharray", "2");

		svg.selectAll(".bar")
			.data(data)
			.enter().append("rect")
			.attr("class", "bar")
			.attr("x", function (d) {
				return d.value < 0 ? x(d.value) : x(0);
			})
			.attr("width", function (d) {
				return d.value < 0 ? x(d.value * -1) - x(0) : x(d.value) - x(0);
			})
			.attr("y", function (d) {
				return y(d.name);
			})
			.attr("height", y.bandwidth())
			.attr("fill", function (d) {
				return d.value < 0 ? "#b2182b" : "#2166ac";
			});
			// .on("mouseover", function (d) {
			// 	div.transition()
			// 		.duration(200)
			// 		.style("opacity", .9);
			// 	div.html(`${d.name}<br>${d.value}`)
			// 		.style("left", (d3.event.pageX - document.getElementById('barplot-main').getBoundingClientRect().x + 40) + "px")
			// 		.style("top", (d3.event.pageY - document.getElementById('barplot-main').getBoundingClientRect().y + 40) + "px");
			// })
			// .on("mouseout", function (d) {
			// 	div.transition()
			// 		.duration(500)
			// 		.style("opacity", 0);
			// });



		//names of the rbps
		svg.selectAll(".name")
			.data(data)
			.enter().append("text")
			.attr("class", "name")
			.attr("x", function (d) {
				return d.value < 0 ? x(0) + 2.55 : x(0) - 2.55
			})
			.attr("y", function (d) {
				return y(d.name);
			})
			.attr("dy", y.bandwidth() - 2.55)
			.attr("text-anchor", function (d) {
				return d.value < 0 ? "start" : "end";
			})
			.style("font-size", "11px")
			.text(function (d) {
				return d.name;
			});

		svg.append("line")
			.attr("x1", x(0))
			.attr("x2", x(0))
			.attr("y1", margin.top)
			.attr("y2", height - margin.top + 20)
			.attr("stroke", "#3a403d")
			.attr("stroke-width", "1px");

		// title
		header.append("text")
			.attr("x", x(0))
			.attr("y", margin.top + 10)
			.style("font-size", "22px")
			.attr("text-anchor", "middle")
			.text("RBFOX2 Expression across Tissues");

		// title
		header.append("text")
			.attr("x", x(0))
			.attr("y", margin.top + 30)
			.style("font-size", "16px")
			.attr("text-anchor", "middle")
			.text("arcsinh(TPM)");

		// scale
		header.append("line")
			.attr("x1", x(0))
			.attr("x2", x(max_val))
			.attr("y1", margin.top + 50)
			.attr("y2", margin.top + 50)
			.attr("stroke", "#3a403d")
			.attr("stroke-width", "1px");

		// scale points + little lines
		header.append("text")
			.attr("x", x(0))
			.attr("y", margin.top + 42)
			.style("font-size", "9px")
			.attr("text-anchor", "middle")
			.text("0.0");

		header.append("text")
			.attr("x", x(max_val))
			.attr("y", margin.top + 42)
			.style("font-size", "9px")
			.attr("text-anchor", "middle")
			.text(max_val.toFixed(3));
		header.append("line")
			.attr("x1", x(max_val))
			.attr("x2", x(max_val))
			.attr("y1", margin.top + 50)
			.attr("y2", margin.top + 55)
			.attr("stroke", "#3a403d")
			.attr("stroke-width", "1px");



	};
	side_plot();

	/*
		--- Main Plot
	 */

	function main_plot() {
		let data = [];
		for (let val of data_incoming['res']) {
			data.push({name: val[0], value: val[1], ctrl_tpm:val[2], kd_tpm: val[3]});
		}

		const header_height = 70;

		var margin = {top: 10, right: 50, bottom: 0, left: 50},
			width = ($('#barplot').parent().width() * (1/2)) - margin.left - margin.right,
			height = 700 - margin.top - margin.bottom + header_height;

		var div = d3.select("#barplot-main").append("div")
			.attr("class", "tooltip")
			.style("background-color", "rgb(187,187,187)")
			.style("font-size", "18px")
			.style("border-radius", "5px")
			.style("padding", "2px")
			.style("opacity", 0)
			.style("pointer-events", "none");

		var outer = d3.select("#barplot-main").append("svg")
			.attr("width", width + margin.left + margin.right)
			.attr("height", height + margin.top + margin.bottom + header_height);

		var header = outer.append("g")
			.attr("transform", "translate(" + margin.left + "," + margin.top + ")");

		var svg = outer.append("g")
			.attr("transform", "translate(" + margin.left + "," + (margin.top + header_height) + ")");

		var x = d3.scaleLinear()
			.range([0, width]);

		const max_val = d3.max(data, function (d) {
			return Math.abs(d.value)
		});

		var y = d3.scaleBand()
			.rangeRound([height, 0])
			.padding(0.2);

		// data.sort(function (a, b) {
		// 	return b.value - a.value;
		// });


		x.domain([-1 * max_val, max_val]);
		y.domain(data.map(function (d) {
			return d.name;
		}));

		svg.selectAll(".connectline")
			.data(data)
			.enter().append("line")
			.attr("x1", -2000)
			.attr("x2", x(0))
			.attr("y1", function (d) {
				return y(d.name) + (y.bandwidth() / 2);
			})
			.attr("y2", function (d) {
				return y(d.name) + (y.bandwidth() / 2);
			})
			.attr("stroke", "#3a403d")
			.attr("stroke-width", "1px")
			.attr("stroke-dasharray", "2");

		svg.selectAll(".bar")
			.data(data)
			.enter().append("rect")
			.attr("class", "bar")
			.attr("x", function (d) {
				return d.value < 0 ? x(d.value) : x(0);
			})
			.attr("width", function (d) {
				return d.value < 0 ? x(d.value * -1) - x(0) : x(d.value) - x(0);
			})
			.attr("y", function (d) {
				return y(d.name);
			})
			.attr("height", y.bandwidth())
			.attr("fill", function (d) {
				return d.value < 0 ? "#b2182b" : "#2166ac";
			})
			.on("mouseover", function (d) {
				div.transition()
					.duration(200)
					.style("opacity", .9);
				div.html(`<h4>${d.name}</h4><dl>
						  <dt>Knockdown</dt><dd>${d.value.toFixed(3)}</dd>
						  <dt>Control TPM</dt><dd>${d.ctrl_tpm.toFixed(3)}</dd>
						  <dt>Knockdown TPM</dt><dd>${d.kd_tpm.toFixed(3)}</dd>
						  </dl>`)
					.style("left", d3.mouse(svg.node())[0]+70 + "px")
					.style("top", d3.mouse(svg.node())[1] + "px");
			})
			.on("mouseout", function (d) {
				div.transition()
					.duration(500)
					.style("opacity", 0);
			});

		// svg.selectAll(".value")
		// 		.data(data)
		// 	.enter().append("text")
		// 		.attr("class", "value")
		// 		.attr("x", function(d){
		// 			if (d.value < 0){
		// 				return (x(d.value * -1) - x(0)) > 20 ? x(d.value) + 2 : x(d.value) - 1;
		// 			} else {
		// 				return (x(d.value) - x(0)) > 20 ? x(d.value) - 2 : x(d.value) + 1;
		// 			}
		// 		})
		// 		.attr("y", function(d){ return y(d.name); })
		// 		.attr("dy", y.bandwidth() - 2.55)
		// 		.attr("text-anchor", function(d){
		// 			if (d.value < 0){
		// 				return (x(d.value * -1) - x(0)) > 20 ? "start" : "end";
		// 			} else {
		// 				return (x(d.value) - x(0)) > 20 ? "end" : "start";
		// 			}
		// 		})
		// 		.style("fill", function(d){
		// 			if (d.value < 0){
		// 				return (x(d.value * -1) - x(0)) > 20 ? "#fff" : "#3a403d";
		// 			} else {
		// 				return (x(d.value) - x(0)) > 20 ? "#fff" : "#3a403d";
		// 			}
		// 		})
		// 		.text(function(d){ return d.value; });

		//names of the rbps
		// svg.selectAll(".name")
		// 	.data(data)
		// 	.enter().append("text")
		// 	.attr("class", "name")
		// 	.attr("x", function (d) {
		// 		return d.value < 0 ? x(0) + 2.55 : x(0) - 2.55
		// 	})
		// 	.attr("y", function (d) {
		// 		return y(d.name);
		// 	})
		// 	.attr("dy", y.bandwidth() - 2.55)
		// 	.attr("text-anchor", function (d) {
		// 		return d.value < 0 ? "start" : "end";
		// 	})
		// 	.style("font-size", "11px")
		// 	.text(function (d) {
		// 		return d.name;
		// 	});

		svg.append("line")
			.attr("x1", x(0))
			.attr("x2", x(0))
			.attr("y1", margin.top)
			.attr("y2", height - margin.top + 20)
			.attr("stroke", "#3a403d")
			.attr("stroke-width", "1px");

		// title
		header.append("text")
			.attr("x", x(0))
			.attr("y", margin.top + 10)
			.style("font-size", "22px")
			.attr("text-anchor", "middle")
			.text("Log2 Fold Change in RBFOX2 Expression upon CELF2 Knockdown");

		// title
		header.append("text")
			.attr("x", x(0))
			.attr("y", margin.top + 30)
			.style("font-size", "16px")
			.attr("text-anchor", "middle")
			.text("log2(Knockdown/Control)");

		// scale
		header.append("line")
			.attr("x1", x(-1*max_val))
			.attr("x2", x(max_val))
			.attr("y1", margin.top + 50)
			.attr("y2", margin.top + 50)
			.attr("stroke", "#3a403d")
			.attr("stroke-width", "1px");

		// scale points + little lines
		header.append("text")
			.attr("x", x(-1*max_val))
			.attr("y", margin.top + 42)
			.style("font-size", "9px")
			.attr("text-anchor", "middle")
			.text((-1*max_val).toFixed(3));
		header.append("line")
			.attr("x1", x(-1*max_val))
			.attr("x2", x(-1*max_val))
			.attr("y1", margin.top + 50)
			.attr("y2", margin.top + 55)
			.attr("stroke", "#3a403d")
			.attr("stroke-width", "1px");

		header.append("text")
			.attr("x", x(0))
			.attr("y", margin.top + 42)
			.style("font-size", "9px")
			.attr("text-anchor", "middle")
			.text("0.0");

		header.append("text")
			.attr("x", x(max_val))
			.attr("y", margin.top + 42)
			.style("font-size", "9px")
			.attr("text-anchor", "middle")
			.text(max_val.toFixed(3));
		header.append("line")
			.attr("x1", x(max_val))
			.attr("x2", x(max_val))
			.attr("y1", margin.top + 50)
			.attr("y2", margin.top + 55)
			.attr("stroke", "#3a403d")
			.attr("stroke-width", "1px");



		// svg.selectAll(".bar")
		//     .data(data)
		//     .enter().append("rect")
		//     .attr("class", function(d) {
		//
		//         if (d[1] < 0){
		//             return "bar negative";
		//         } else {
		//             return "bar positive";
		//         }
		//
		//     })
		//     .attr("data-yr", function(d){
		//         return d[0];
		//     })
		//     .attr("data-c", function(d){
		//         return d[1];
		//     })
		//     .attr("title", function(d){
		//         return (d.Year + ": " + d[1] + " °C")
		//     })
		//     .attr("y", function(d) {
		//
		//         if (d[1] > 0){
		//             return y(d[1]);
		//         } else {
		//             return y(0);
		//         }
		//
		//     })
		//     .attr("x", function(d) {
		//         return x(d.Year);
		//     })
		//     .attr("width", x.rangeBand())
		//     .attr("height", function(d) {
		//         return Math.abs(y(d[1]) - y(0));
		//     })
		//     .on("mouseover", function(d){
		//         // alert("Year: " + d.Year + ": " + d[1] + " Celsius");
		//         d3.select("#_yr")
		//             .text("Year: " + d[0]);
		//         d3.select("#degrree")
		//             .text(d[1] + "°C");
		//     });

	}
	main_plot();




}