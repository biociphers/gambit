
// Cytoscape related ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

var cy = null;
var cylayout = null;

//cytoscape.use(cola);



async function plot_cyto(initial_rbp, initial_tissue, callback){
    // on change, reload the graph
    const initial_id = initial_rbp;
    //uniq_counter++;
    const initial_elements = [{ data: { id: initial_id, name: initial_rbp}}];  // just the node with no edges
    const parent = $('#cy').parent();

    if(cy){
        cy.destroy();
    }
    $('#cy').remove();
    $(parent).append('<div id="cy"></div>');

    const base_color = ['#00FF00', '#00AA00'];
    const visited_color = ['#0000FF', '#0000AA'];
    const highlight_color = ['#FF00FF', '#AA00AA'];

    cy = cytoscape({
        container: document.getElementById('cy'),
        elements: initial_elements,
          style: [
            {
              selector: 'edge',
              style: {
                  'curve-style': 'bezier',
                  'width': 'mapData(weight, 0.0, 1.0, 1.0, 4.0)',
                  'mid-target-arrow-shape': 'triangle',
                  'arrow-scale': 'mapData(weight, 0.0, 1.0, 1.0, 2.0)'
              }
            },{
              selector: 'edge.downreg',
              style: {
                  'line-color': '#b2182b',
                  'opacity': 'mapData(weight, 0.0, 1.0, 0.2, 1.0)',
                  'mid-target-arrow-color': '#b2182b',
              }
            },{
              selector: 'edge.upreg',
              style: {
                  'line-color': '#2166ac',
                  'opacity': 'mapData(weight, 0.0, 1.0, 0.2, 1.0)',
                  'mid-target-arrow-color': '#2166ac',
              }
            },
            {
                selector: 'node',
                style: {
                    shape: 'ellipse',
                    'background-color': '#a6cee3',
                    label: 'data(name)',
                    "text-valign" : "center",
                    "text-halign" : "center",
                    'text-opacity': 1.0,
                    // "text-background-color": "#a6cee3",
                    // "text-background-opacity": "1",
                    // "text-background-padding": "2",
                    "padding": "10px",
                    "width": "label",
                    "height": "label"

                    }
            },
            {
                selector: 'node.visited',
                style: {
                    //shape: 'hexagon',
                    'background-color': '#1f78b4',
                    // "text-background-color": "#1f78b4",
                    }
            },
            {
                selector: 'node.selected',
                style: {
                    //shape: 'hexagon',
                    'background-color': '#b2df8a',
                    // "text-background-color": "#b2df8a",
                    }
            },
            {
                selector: 'node.text-hidden',
                style: {
                    'text-opacity': 0.0,
                    // "text-background-opacity": 0.0,
                    }
            }


        ],

      });

    sendJS({'rbp1': initial_rbp, 'tissue': initial_tissue}, '/json/get_graph_tissue_rbp', function(r){


        cy.add(newnodes2newobjects(r, initial_id));
        cy.$('#' + initial_id).addClass('visited').addClass('selected');

        cylayout = cy.makeLayout({'name': 'cola'});
        cylayout.run();
        // cy.layout({
        //      'name': 'circle'
        // });
        cy.fit();
    });

    cy.on('tap', 'node', function(evt){

        cy.nodes().removeClass('text-hidden');
        labelsHidden = false;
        // when clicking on a node, we remove other elements which don't have connections
        // then we drill down to the connections of that new node and spawn them
        // console.log( 'clicked ' + this.id() );
        // console.log(this);
        // console.log(evt);
        // console.log(this.connectedEdges())
        const clicked_node = this;
        const add_to_id = this.id();
        // if(this.connectedEdges().length > 1){
        //     console.error("more than one edge");
        //     return
        // }
        // find parent
        const connecting_edge = this.connectedEdges()[0];
        const parent = connecting_edge.source();
        //console.log('parent', parent, parent.outgoers());


        // remove all of the other edges / nodes except the clicked one
        // $.each(parent.connectedEdges(), function(i, v){
        //     console.log(i, v.id(), connecting_edge.id(), v.target().id());
        //     if(!(v.id() === connecting_edge.id())){
        //         cy.remove(v.target());
        //     }
        // })

        // callback method to add new nodes
        // there should be an ajax here but we just skip it for clarity

        var in_view = [parent];

        cy.nodes().removeClass('selected');
        clicked_node.addClass('visited').addClass('selected');

        sendJS({'rbp1': add_to_id, 'tissue': initial_tissue}, '/json/get_graph_tissue_rbp',  function(r){
            const added_nodes = cy.add(newnodes2newobjects(r, add_to_id));
            console.log(added_nodes)
            cylayout.stop();
            // cylayout = cy.makeLayout({'name': 'cola',
            // fit: false, animate:true, maxSimulationTime:400, infinite:true});
            cylayout = cy.makeLayout({'name': 'cola',
                nodeDimensionsIncludeLabels: true, nodeSpacing: function( node ){ return 30; }
            });
            //const disp_nodes = added_nodes.union(parent);
            // cylayout.one('layoutstop', function(){
            //     console.log('ready')
            //     //cy.center(added_nodes);
            //     cy.fit(added_nodes, 50);
            // });

            // cylayout.one('layoutready', function(){
            //     setTimeout(function(){
            //         cy.animate({
            //           fit: {
            //             eles: disp_nodes,
            //             padding: 100
            //           }
            //         }, {
            //           duration: 300
            //         });
            //         //cylayout.stop(); // stop cpu usage?
            //     }, 450)
            //
            // });

            cylayout.run();
            //cy.fit(added_nodes, 50);





        });




    });

    if(callback){
        callback();
    }
    //cy.fit();

};


    function newnodes2newobjects(nodes, origin_node_id){
        // convert a list of just nodes names/ids and the id of some origin node into a proper list ob nodes and edges
        // for cytoscape to add with
        var elem_list = [];

        $.each(nodes, function(i, v){
            const rbp = v[0], weight1=v[1], weight2=v[2];
            if(rbp !== origin_node_id){
                console.log(weight1, weight2)
                elem_list.push({group: 'nodes', data: { id: rbp, name: rbp}});

                const outgoing = {group: 'edges', data: { id: `${origin_node_id}-${rbp}`,
                        source: origin_node_id, target: rbp, weight: Math.abs(weight1)}};

                outgoing.classes = weight1 < 0 ? ['downreg'] : ['upreg'];

                const incoming = {group: 'edges', data: { id: `${rbp}-${origin_node_id}`,
                        source: rbp, target: origin_node_id, weight: Math.abs(weight2)}};

                incoming.classes = weight2 < 0 ? ['downreg'] : ['upreg'];

                //
                // console.log(outgoing)
                // console.log(incoming)
                elem_list.push(outgoing);
                elem_list.push(incoming);
            }

            //uniq_counter++;
        });


        return elem_list;
    }

let labelsHidden = false;
function toggleLabels(){
    if(cy !== undefined){
        if(labelsHidden === true){
            console.log(1)
            cy.nodes().removeClass('text-hidden');
            labelsHidden = false;
        }else{
            console.log(2)
            cy.nodes().addClass('text-hidden');
            labelsHidden = true;
        }
    }
}
$('#cyto-toggle-labels-btn').click(toggleLabels);

