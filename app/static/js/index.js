
function sendJS(obj, path, callback){
    return $.ajax({
      type: "POST",
      contentType: "application/json; charset=utf-8",
      url: base_url + path,
      data: JSON.stringify(obj),
      success: callback,
      dataType: "json"
    });
}

function canvasplot(rawdata, x_labels, y_labels, cluster_lens){

    // here we still use d3 helpers for axis bandwidths but do the rest of the drawing manually on the canvas

    $('#mainplot').remove();
    $('#mainplot-panel').append('<canvas id="mainplot">');

    var margin = {top: 0, right: 0, bottom: 0, left: 0},
      width = $('#mainplot-panel').width() - margin.left - margin.right,
      height = $('#mainplot-panel').width() - margin.top - margin.bottom;
    const topbar_height = 10;
    const x_axis_margin = 20;
    const y_axis_margin = 20;

    $('#mainplot').attr('width', width).attr('height', height + topbar_height + x_axis_margin);

    // Build X scales and axis:
    var xs = d3.scaleBand()
      .range([ 0, width ])
      .domain(x_labels)
      .padding(0.01);
    // svg.append("g")
    //   .attr("transform", "translate(0," + height + ")")
    //   .call(d3.axisBottom(xs));

    var ys = d3.scaleBand()
      .range([ 0, height ])
      .domain(y_labels)
      .padding(0.01);
    // svg.append("g")
    //   .call(d3.axisLeft(y));

    var colorScale = d3.scaleLinear()
      .range(["#E2E4CC", "#AECFA2", "#75B490", "#4A848B", "#3C5176", "#31274B"])
      .domain([0.0, 0.2, 0.4, 0.6, 0.8, 1.0]);

    const canvas = $('#mainplot')[0];
    const ctx = canvas.getContext('2d');


    // draw top bars

    ctx.save();
    ctx.translate(y_axis_margin, 0);
    cluster_lens.forEach(function(d, i) {

        let x = xs(d[0]),
            y = 0,
            width = xs(d[1]) - xs(d[0]) + xs.bandwidth(),
            height = topbar_height;

        ctx.fillStyle = i % 2 ? "green" : "red";
        ctx.fillRect(x, y, width , height);

    });
    ctx.restore();

    // draw x axis label
    ctx.save();
    ctx.translate(0, topbar_height + height);
    ctx.font="14px monospace";
    ctx.textAlign="center";
    ctx.textBaseline = "middle";
    ctx.fillStyle = "#000000";
    ctx.fillText("Samples",(width/2),(x_axis_margin/2));
    ctx.restore();

    // draw y axis label
    ctx.save();
    ctx.translate(0, topbar_height);
    ctx.translate(y_axis_margin/2, height/2);
    ctx.rotate(-90 * Math.PI / 180);
    ctx.font="14px monospace";
    ctx.textAlign="center";
    ctx.textBaseline = "middle";
    ctx.fillStyle = "#000000";
    ctx.fillText("LSVs",0 , 0,);
    ctx.restore();

    ctx.save();
    ctx.translate(y_axis_margin, topbar_height);
    // draw main plot
    rawdata.forEach(function(row, i){
        row.forEach(function(col, j){

            //csvData.push({'x':x_labels[i], 'y':y_labels[j], 'v':col})
            let x = xs(x_labels[i]),
                y = ys(y_labels[j]),
                width = xs.bandwidth(),
                height = ys.bandwidth();

            ctx.fillStyle = colorScale(col);
            ctx.fillRect(x, y, width , height);

        });
    });
    ctx.restore();


    console.log('c complete')

    $('#mainPanel .panel-heading').eq(0).removeClass('loader-background');

    let last_clientX = null;
    let last_clientY = null;
    let last_radius = parseInt($('#selection_radius').val());

    function replot_sideplot(){
        if (last_clientX === null){
            return;
        }
        $('#mainplot-overlay').remove();
        $('#mainplot-panel').append('<canvas id="mainplot-overlay">');
        $('#mainplot-overlay').attr('width', width).attr('height', height + topbar_height + x_axis_margin);

        console.log(last_clientX, last_clientY)

        const rect = canvas.getBoundingClientRect()
        const relX = last_clientX- rect.left - y_axis_margin;
        const relY = last_clientY - rect.top - topbar_height;
        //console.log("x: " + x + " y: " + y)

        const eachBandX = xs.step();
        const indexX = Math.floor((relX / eachBandX));
        //const valX = xs.domain()[indexX];

        const eachBandY = ys.step();
        const indexY = Math.floor((relY / eachBandY));
        //const valY = ys.domain()[indexY];


        const radius = last_radius;
        let lower_bound_x, upper_bound_x, lower_bound_y, upper_bound_y;
        if(indexX - radius < 0){
            lower_bound_x = 0;
            upper_bound_x = radius * 2;
        }else if(indexX + radius >= x_labels.length){
            lower_bound_x = x_labels.length - (radius * 2) - 1;
            upper_bound_x = x_labels.length - 1;
        }else{
            lower_bound_x = indexX - radius;
            upper_bound_x = indexX + radius;
        }

        if(indexY - radius < 0){
            lower_bound_y = 0;
            upper_bound_y = radius * 2;
        }else if(indexY + radius >= y_labels.length){
            lower_bound_y = y_labels.length - (radius * 2) - 1;
            upper_bound_y = y_labels.length - 1;
        }else{
            lower_bound_y = indexY - radius;
            upper_bound_y = indexY + radius;
        }

        const ov_ctx = $('#mainplot-overlay')[0].getContext('2d');

        ov_ctx.translate(y_axis_margin, topbar_height);
        ov_ctx.strokeStyle = 'red';
        ov_ctx.lineWidth = 3;
        //ctx.fillStyle = 'red';
        ov_ctx.strokeRect(xs(x_labels[lower_bound_x]), ys(y_labels[lower_bound_y]), xs.bandwidth() * (radius * 2 + 1), ys.bandwidth() * (radius * 2 + 1));
        // console.log(indexX, indexY, valX, valY)
        // console.log(lower_bound_x, lower_bound_y, upper_bound_x, upper_bound_y)
        // console.log(xs(x_labels[lower_bound_x]), ys(y_labels[lower_bound_y]))


        const sliced = slice_rawdata(rawdata, x_labels, y_labels, lower_bound_x, upper_bound_x, lower_bound_y, upper_bound_y);

        setTimeout(function(){on_sideplot_update(sliced[0], sliced[1], sliced[2])}, 0);
    }

    $('#selection_radius').unbind().change(function(){
        const new_radius = parseInt($(this).val());
        $('#selection_radius2').val(new_radius);
        last_radius = new_radius;
        replot_sideplot();
    });
    $('#selection_radius2').unbind().change(function(){
        const new_radius = parseInt($(this).val());
        $('#selection_radius').val(new_radius);
        last_radius = new_radius;
        replot_sideplot();
    });

    $('#mainplot')[0].addEventListener('mousedown', function(e) {

        last_clientX = e.clientX;
        last_clientY = e.clientY;
        replot_sideplot();

    })

    // $('#mainplot').click(function(e){
    //     var parentOffset = $(this).parent().offset();
    //    //or $(this).offset(); if you really just want the current element's offset
    //
    //
    //     var relX = e.pageX - parentOffset.left - y_axis_margin;
    //     var relY = e.pageY - parentOffset.top - topbar_height;
    //
    //     console.log(e.pageX - parentOffset.left, e.pageY - parentOffset.top, relX, relY)
    //
    //     const eachBandX = xs.step();
    //     const indexX = Math.round((relX / eachBandX));
    //     const valX = xs.domain()[indexX];
    //
    //     const eachBandY = ys.step();
    //     const indexY = Math.round((relY / eachBandY));
    //     const valY = ys.domain()[indexY];
    //
    //     console.log(indexX, indexY, valX, valY)
    //
    //
    //
    // })

}

function on_sideplot_update(rawdata, x_labels, y_labels){
    $('#sideplot').remove();
    $('#sideplot-panel').append('<div id="sideplot">');

    var margin = {top: 20, right: 20, bottom: 50, left: 250},
      width = $('#sideplot-panel').width() - margin.left - margin.right,
      height = $('#sideplot-panel').width() - margin.top - margin.bottom;

    // append the svg object to the body of the page
    var svg = d3.select("#sideplot")
    .append("svg")
      .attr("width", width + margin.left + margin.right)
      .attr("height", height + margin.top + margin.bottom)
    .append("g")
      .attr("transform",
            "translate(" + margin.left + "," + margin.top + ")");

    var csvData = [];

    rawdata.forEach(function(row, i){
        row.forEach(function(col, j){
            csvData.push({'x':x_labels[i], 'y':y_labels[j], 'v':col, 'i': i, 'j':j})
        })
    });

    // Build X scales and axis:
    var xs = d3.scaleBand()
      .range([ 0, width ])
      .domain(x_labels)
      .padding(0.01);
    svg.append("g")
      .attr("transform", "translate(0," + height + ")")
      .call(d3.axisBottom(xs))
    .selectAll("text")
        .style("text-anchor", "end")
        .attr("dx", "-.8em")
        .attr("dy", ".15em")
        .attr("transform", "rotate(-65)");

    // Build X scales and axis:
    var ys = d3.scaleBand()
      .range([ 0, height ])
      .domain(y_labels)
      .padding(0.01);
    svg.append("g")
      .call(d3.axisLeft(ys));

    // Build color scale
    var colorScale = d3.scaleLinear()
      .range(["#E2E4CC", "#AECFA2", "#75B490", "#4A848B", "#3C5176", "#31274B"])
      .domain([0.0, 0.2, 0.4, 0.6, 0.8, 1.0]);

    //Read the data

      svg.selectAll()
          .data(csvData, function(d) {return d.x+':'+d.y;})
          .enter()
          .append("rect")
          .attr("x", function(d) { return xs(d.x) })
          .attr("y", function(d) { return ys(d.y) })
          .attr("width", xs.bandwidth() )
          .attr("height", ys.bandwidth() )
          .style("fill", function(d) { return colorScale(d.v)} )
          .append("svg:title")
          .text(function(d, i) { return`${d.x} ; ${d.y} ; ${d.v}`});

    console.log('c2 complete')

};

function on_success(rawdata, x_labels, y_labels, cluster_lens){
    //  window.inchlib = new InCHlib({ //instantiate InCHlib
    //     target: "inchlib", //ID of a target HTML element
    //     metadata: false, //turn on the metadata
    //     column_metadata: false, //turn on the column metadata
    //     max_height: 800, //set maximum height of visualization in pixels
    //     width: 1100, //set width of visualization in pixels
    //     heatmap_colors: "RdBu", //set color scale for clustered data
    //     metadata_colors: "Reds", //set color scale for metadata
    // });
    //
    // inchlib.read_data(data); //read input json file
    // await inchlib.draw(); //draw cluster heatmap
    $('#mainplot').remove();
    $('#mainplot-panel').append('<div id="mainplot">');


    // set the dimensions and margins of the graph
    var margin = {top: 30, right: 30, bottom: 30, left: 30},
      width = $('#mainplot-panel').width() - margin.left - margin.right,
      height = $('#mainplot-panel').width() - margin.top - margin.bottom;

    // append the svg object to the body of the page
    var svg = d3.select("#mainplot")
    .append("svg")
      .attr("width", width + margin.left + margin.right)
      .attr("height", height + margin.top + margin.bottom)
    .append("g")
      .attr("transform",
            "translate(" + margin.left + "," + margin.top + ")");

    var csvData = [];

    rawdata.forEach(function(row, i){
        row.forEach(function(col, j){
            csvData.push({'x':x_labels[i], 'y':y_labels[j], 'v':col})
        })
    });

    // Build X scales and axis:
    var x = d3.scaleBand()
      .range([ 0, width ])
      .domain(x_labels)
      .padding(0.01);
    svg.append("g")
      .attr("transform", "translate(0," + height + ")")
      .call(d3.axisBottom(x));

    // top bar for clusters
    // Build color scale

    svg.selectAll('g.topbar')
        .data(cluster_lens)
        .enter()
        .append('rect')
        .attr("x", function(d) { return x(d[0]) })
        .attr("y", function(d) { return -10 })
        .attr("width", function(d){ return (x(d[1]) - x(d[0])) + x.bandwidth() })
        .attr("height", 10 )
        .style("fill", function(d, i) { return i % 2 ? "green" : "red"} );

    // Build X scales and axis:
    var y = d3.scaleBand()
      .range([ height, 0 ])
      .domain(y_labels)
      .padding(0.01);
    svg.append("g")
      .call(d3.axisLeft(y));

    // Build color scale
    var myColor = d3.scaleLinear()
      .range(["#E2E4CC", "#AECFA2", "#75B490", "#4A848B", "#3C5176", "#31274B"])
      .domain([0.0, 0.2, 0.4, 0.6, 0.8, 1.0]);

    //Read the data

      svg.selectAll()
          .data(csvData, function(d) {return d.x+':'+d.y;})
          .enter()
          .append("rect")
          .attr("x", function(d) { return x(d.x) })
          .attr("y", function(d) { return y(d.y) })
          .attr("width", x.bandwidth() )
          .attr("height", y.bandwidth() )
          .style("fill", function(d) { return myColor(d.v)} );

    console.log('c complete')



    $('#mainPanel .panel-heading').eq(0).removeClass('loader-background');
}

function refresh_plot(){
    if(!$('#input_matrix').val() || !$('#output_matrix').val()){
        return;
    }

    $('#mainPanel .panel-heading').eq(0).addClass('loader-background');
    var formData = new FormData();
    formData.append('input_matrix', $('#input_matrix')[0].files[0]);
    formData.append('output_matrix', $('#output_matrix')[0].files[0]);



    $.ajax({
           url : '/json/data_proc',
           type : 'POST',
           data : formData,
           processData: false,  // tell jQuery not to process the data
           contentType: false,  // tell jQuery not to set contentType
           success : function(r) {
               console.log('what?')
               console.log(r)



               //on_success(r['rawdata'], r['x_labels'], r['y_labels'], r['cluster_lens']);
               canvasplot(r['rawdata'], r['x_labels'], r['y_labels'], r['cluster_lens']);
           }
    });
}


function slice_rawdata(_rawdata, _x_labels, _y_labels, lower_bound_x, upper_bound_x, lower_bound_y, upper_bound_y){

    const x_labels = _x_labels.slice(lower_bound_x, upper_bound_x + 1);
    const y_labels = _y_labels.slice(lower_bound_y, upper_bound_y + 1);
    let rawdata = [];

    for(let i = lower_bound_x; i < upper_bound_x + 1; i++){
        rawdata.push(_rawdata[i].slice(lower_bound_y, upper_bound_y + 1));
    }

    return [rawdata, x_labels, y_labels];
};

$('#input_matrix').change(refresh_plot);
$('#output_matrix').change(refresh_plot);

// on_success([[0.1,0.2,0.3],[0.4,0.5,0.6],[0.7,0.8,0.9]], ["A", "B", "C", ], ["v1", "v2", "v3"],
//     [["A", "B"], ["C","C"]]);
//
// var formData = new FormData();
// $.ajax({
//            url : '/json/data_proc',
//            type : 'POST',
//            data : formData,
//            processData: false,  // tell jQuery not to process the data
//            contentType: false,  // tell jQuery not to set contentType
//            success : function(data) {
//                 //console.log(data)
//                 canvasplot(data.rawdata, data.x_labels, data.y_labels, data.cluster_lens)
//
//                const lower_bound = 0;
//                const upper_bound = 100;
//
//                //const sliced = slice_rawdata(data.rawdata, data.x_labels, data.y_labels, lower_bound, upper_bound, lower_bound, upper_bound);
//                //canvasplot(sliced[0], sliced[1], sliced[2], data.cluster_lens);
//
//                //on_sideplot_update(rawdata, x_labels, y_labels)
//                //canvasplot(rawdata, x_labels, y_labels, data.cluster_lens)
//
//            }
//     });



//
// sendJS({}, '/json/datatest', function(r){
//     window.inchlib = new InCHlib({ //instantiate InCHlib
//         target: "inchlib", //ID of a target HTML element
//         metadata: false, //turn on the metadata
//         column_metadata: false, //turn on the column metadata
//         max_height: 800, //set maximum height of visualization in pixels
//         width: 1100, //set width of visualization in pixels
//         heatmap_colors: "RdBu", //set color scale for clustered data
//         metadata_colors: "Reds", //set color scale for metadata
//     });
//
//     inchlib.read_data(r); //read input json file
//     inchlib.draw(); //draw cluster heatmap
// });

