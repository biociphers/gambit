
function saveSvg(svgEl, name) {
    svgEl.setAttribute("xmlns", "http://www.w3.org/2000/svg");
    var svgData = svgEl.outerHTML;
    var preface = '<?xml version="1.0" standalone="no"?>\r\n';
    var svgBlob = new Blob([preface, svgData], {type:"image/svg+xml;charset=utf-8"});
    var svgUrl = URL.createObjectURL(svgBlob);
    var downloadLink = document.createElement("a");
    downloadLink.href = svgUrl;
    downloadLink.download = name;
    document.body.appendChild(downloadLink);
    downloadLink.click();
    document.body.removeChild(downloadLink);
}



function copyStylesInline(destinationNode, sourceNode) {
   var containerElements = ["svg","g"];
   for (var cd = 0; cd < destinationNode.childNodes.length; cd++) {
       var child = destinationNode.childNodes[cd];
       if (containerElements.indexOf(child.tagName) != -1) {
            copyStylesInline(child, sourceNode.childNodes[cd]);
            continue;
       }
       var style = sourceNode.childNodes[cd].currentStyle || window.getComputedStyle(sourceNode.childNodes[cd]);
       if (style == "undefined" || style == null) continue;
       for (var st = 0; st < style.length; st++){
            child.style.setProperty(style[st], style.getPropertyValue(style[st]));
       }
   }
}

function triggerDownload (imgURI, fileName) {
  var evt = new MouseEvent("click", {
    view: window,
    bubbles: false,
    cancelable: true
  });
  var a = document.createElement("a");
  a.setAttribute("download", fileName);
  a.setAttribute("href", imgURI);
  a.setAttribute("target", '_blank');
  a.dispatchEvent(evt);
}

function savePng(svg, fileName) {
  var copy = svg.cloneNode(true);
  copyStylesInline(copy, svg);
  var canvas = document.createElement("canvas");
  var bbox = svg.getBBox();
  if (bbox.width === 0) bbox.width = svg.getAttribute('width');
  if (bbox.height === 0) bbox.height = svg.getAttribute('height');
  canvas.width = bbox.width;
  canvas.height = bbox.height;
  var ctx = canvas.getContext("2d");
  ctx.clearRect(0, 0, bbox.width, bbox.height);
  var data = (new XMLSerializer()).serializeToString(copy);
  var DOMURL = window.URL || window.webkitURL || window;
  var img = new Image();
  var svgBlob = new Blob([data], {type: "image/svg+xml;charset=utf-8"});
  var url = DOMURL.createObjectURL(svgBlob);
  img.onload = function () {
    ctx.drawImage(img, 0, 0);
    DOMURL.revokeObjectURL(url);
    if (typeof navigator !== "undefined" && navigator.msSaveOrOpenBlob)
    {
        var blob = canvas.msToBlob();
        navigator.msSaveOrOpenBlob(blob, fileName);
    }
    else {
        var imgURI = canvas
            .toDataURL("image/png")
            .replace("image/png", "image/octet-stream");
        triggerDownload(imgURI, fileName);
    }
    document.removeChild(canvas);
  };
  img.src = url;
}

const base64ToBlob = (b64Data, contentType='', sliceSize=512) => {
  const byteCharacters = atob(b64Data);
  const byteArrays = [];

  for (let offset = 0; offset < byteCharacters.length; offset += sliceSize) {
    const slice = byteCharacters.slice(offset, offset + sliceSize);

    const byteNumbers = new Array(slice.length);
    for (let i = 0; i < slice.length; i++) {
      byteNumbers[i] = slice.charCodeAt(i);
    }

    const byteArray = new Uint8Array(byteNumbers);
    byteArrays.push(byteArray);
  }

  const blob = new Blob(byteArrays, {type: contentType});
  return blob;
}

function cy2png(fileName){
    var b64key = 'base64,';
    var b64 = cy.png().substring( cy.png().indexOf(b64key) + b64key.length );
    var imgBlob = base64ToBlob( b64, 'image/png' );

    saveAs( imgBlob, fileName );
}

function savePdf(svg, fileName){
    console.log(svg)
      let doc = new PDFDocument({
            layout : 'landscape',
            size: [svg.height.baseVal.value, svg.width.baseVal.value]
        });
      SVGtoPDF(doc, svg, 0, 0);
      // if (textarea.value) { // Use the SVG code in the textarea
      //   if (document.getElementById('use-css').checked) {
      //     let hiddenDiv = document.getElementById('hidden-div');
      //     hiddenDiv.innerHTML = '<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">' + textarea.value.trim() + '</svg>';
      //     SVGtoPDF(doc, hiddenDiv.firstChild, 0, 0, {useCSS:true});
      //   } else {
      //     SVGtoPDF(doc, textarea.value, 0, 0);
      //   }
      // } else {
      //   let svgs = document.querySelectorAll('#column-1 > svg'); // Use all the svg drawings on the page
      //   for (let i = 0; i < svgs.length; i++) {
      //     doc.fontSize(20).text(svgs[i].id, {underline: true});
      //     SVGtoPDF(doc, svgs[i].outerHTML || svgs[i], 0, 0);
      //     if (i !== svgs.length - 1) {
      //       resetDefaultStyles(doc);
      //       doc.addPage();
      //     }
      //   }
      // }
      let stream = doc.pipe(blobStream());
      stream.on('finish', function() {
        let blob = stream.toBlob('application/pdf');
        saveAs( blob, fileName );
      });
      doc.end();

}



// button triggers

$('#wc-dl-svg-btn').click(function(){
    saveSvg($('#wordcloud').children('svg')[0], `rbp-pokedex-${last_data[0]}-${last_data[1]}.svg`);
});

$('#wc-dl-png-btn').click(function(){
    savePng($('#wordcloud').children('svg')[0], `rbp-pokedex-${last_data[0]}-${last_data[1]}.png`);
});

$('#wc-dl-pdf-btn').click(function(){
    savePdf($('#wordcloud').children('svg')[0], `rbp-pokedex-${last_data[0]}-${last_data[1]}.pdf`);
});

function combine_svgs(svg1, svg2){
    const offset = svg1.width();
    const offset2 = svg2.width();
    const height = svg1.height();
    svg1 = svg1.clone()[0];
    svg2 = svg2.clone()[0];
    let finalSVG = document.createElementNS("http://www.w3.org/2000/svg", "svg");
    finalSVG.setAttribute('width', offset + offset2);
    finalSVG.setAttribute('height', height);
    finalSVG.appendChild(svg1);
    svg2.setAttribute('x', offset);
    finalSVG.appendChild(svg2);


    return finalSVG;
}

$('#bp-dl-svg-btn').click(function(){
    const combined = combine_svgs($('#barplot-side').children('svg').eq(0), $('#barplot-main').children('svg').eq(0));
    saveSvg(combined, `rbp-pokedex-${last_data[0]}-${last_data[1]}.svg`);
});

$('#bp-dl-png-btn').click(function(){
    const combined = combine_svgs($('#barplot-side').children('svg').eq(0), $('#barplot-main').children('svg').eq(0));

    savePng(combined, `rbp-pokedex-${last_data[0]}-${last_data[1]}.png`);
});

$('#bp-dl-pdf-btn').click(function(){
    const combined = combine_svgs($('#barplot-side').children('svg').eq(0), $('#barplot-main').children('svg').eq(0));
    savePdf(combined, `rbp-pokedex-${last_data[0]}-${last_data[1]}.pdf`);
});

// $('#gr-dl-svg-btn').click(function(){
//     saveSvg($('#cy').children('svg')[0], `rbp-pokedex-${last_data[0]}-${last_data[1]}.svg`);
// });

$('#gr-dl-png-btn').click(function(){
    cy2png(`rbp-pokedex-${last_data[0]}-${last_data[1]}.png`)
});