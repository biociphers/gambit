
// Word cloud related ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~



function num2color(num){
    const alpha = (0x7F + (0x7F * Math.abs(num)));
    const hexstr = ('00' + alpha.toString(16).split('.')[0]).substr(-2);

    if(num < 0){
        return '#b2182b' + hexstr;
    }else{

        return '#2166ac' + hexstr;
    }
};


function plot_wordcloud(data_incoming, upper_limit){

    // convert the raw data into normalized form, and then limit it to a range of font sizes

    const lower_limit = 8;
    const max_val = data_incoming['max_val'];
    const min_val = data_incoming['min_val'];
    const data = data_incoming['res'];

    $.each(data, function(i, v){
        const val = v[1];
        const normalized = (Math.abs(val) - min_val) / (max_val - min_val);
        data[i].push(normalized);
        if(val < 0){
            data[i].push(normalized * -1.0);
        }else{
            data[i].push(normalized);
        }
    });

    console.log($('#wordcloud').parent().width())

    var layout = d3.layout.cloud()
        .size([$('#wordcloud').parent().width(), 500])
        .words(data.map(function(d) {

          return {text: d[0], size: lower_limit + d[2]*upper_limit, color:num2color(d[3])};
        }))
        .padding(5)
        //.rotate(function() { return ~~(Math.random() * 3) * 45; })
        .rotate(function() { return 0 })
        .font("Impact")
        .fontSize(function(d) { return d.size*2; })

        .on("end", draw);

    layout.start();

    function draw(words) {

      $('#wordcloud').children().remove();

      d3.select("#wordcloud").append("svg")
          .attr("width", layout.size()[0])
          .attr("height", layout.size()[1])
        .append("g")
          .attr("transform", "translate(" + layout.size()[0] / 2 + "," + layout.size()[1] / 2 + ")")
        .selectAll("text")
          .data(words)
        .enter().append("text")
          .style("font-size", function(d) { return d.size + "px"; })
          .style("font-family", "Impact")
          .style("fill", function(d){return d.color})
          .attr("text-anchor", "middle")
          .attr("transform", function(d) {
            return "translate(" + [d.x, d.y] + ")rotate(" + d.rotate + ")";
          })
          .text(function(d) { return d.text; });
    }
}
